// include fs-extra package
var fs = require("fs-extra");
var path = require('path');
const moment = require('moment')

function getDate() {
  return moment().format('DD-MM-YYYY-HH-mm-ss');
}

function copyfolder(source,folderName, datedFolder){
  var finalDestination;
  if (!fs.existsSync(folderName)) {
    fs.mkdirSync(folderName);
    if(fs.existsSync(folderName)) {
      finalDestination = path.join(folderName , datedFolder);
      fs.mkdirSync(finalDestination);
    }
  }else{
    finalDestination = path.join(folderName , datedFolder);
    fs.mkdirSync(finalDestination);
   
    // console.log("folder already exists in the directory");
  }
  fs.copy(source, finalDestination, function (err) {
    console.log('Copy completed!')
    if (err){
        console.log('An error occured while copying the folder.');
        return console.error(err);
    }
});
}
copyfolder('cypress/reports/allure','cypress/testreport', getDate());