// ***********************************************************
// This example support/e2e.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'
require('cypress-xpath');
require('moment');
require('@shelex/cypress-allure-plugin');
//Srequire('cypress-failed-log');
//require('cypress-terminal-report/src/installLogsCollector')();






// const winston = require('winston');
// const logger = winston.createLogger({
//   level: 'info',
//   format: winston.format.combine(
//     winston.format.timestamp(),
//     winston.format.json()
//   ),
//   transports: [S
//     new winston.transports.File({ filename: 'cypress/logs.log' })
//   ]
// });
// module.exports=logger;

// Alternatively you can use CommonJS syntax:
// require('./commands')