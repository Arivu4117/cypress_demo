const ApplicationKeywords=require('../page_objects/ApplicationKeywords');
const moment = require('moment');
class creatingSupplier {

baseKey =new ApplicationKeywords();
    
    /**
     * created by: Arivarasan A
     * Methode description: creating the supplier
     */
    creationOfSupplier(supplierlabel,internallabel,internalvalue,externallabel,externalvalue,descriptionlabel,descriptionvalue,
                       industrylabel,industryvalue,noOfemployeelabel,noOfemployeevalue,datelabel,address1label,address1value,
                       address2label,address2value,citylabel,cityvalue,statelabel,statevalue,ziplabel,zipvalue,
                       addfilelabel,addfilepathvalue,cfoname,ceoname,hrname,adminname,recurrentsupplierlabel,savebuttonlabel){

        let datevalue=this.baseKey.generateDate();
        let splittedDate=datevalue.split("/");
        let month=this.baseKey.monthinString(splittedDate[1]);
        let year=splittedDate[2];
        let date=splittedDate[0];

        this.baseKey.clickon(supplierlabel);
        this.baseKey.inputTypeIn(internallabel,internalvalue);
        this.baseKey.inputTypeIn(externallabel,externalvalue);
        this.baseKey.inputTypeIn(descriptionlabel,descriptionvalue);
        this.baseKey.selectDropdown(industrylabel,industryvalue);
        this.baseKey.inputTypeIn(noOfemployeelabel,noOfemployeevalue);
        this.baseKey.checkBox(recurrentsupplierlabel);
        // this.baseKey.inputTypeIn(datelabel,datevalue);
        this.selectDate(datelabel,month,year,date);
        this.baseKey.inputTypeIn(address1label,address1value);
        this.baseKey.inputTypeIn(address2label,address2value);
        this.baseKey.inputTypeIn(citylabel,cityvalue);
        this.baseKey.inputTypeIn(statelabel,statevalue);
        this.baseKey.inputTypeIn(ziplabel,zipvalue);
        this.fileUpload(addfilelabel,addfilepathvalue);
        this.baseKey.clickon(cfoname);
        this.baseKey.clickon(ceoname);
        this.baseKey.clickon(hrname);
        this.baseKey.clickon(adminname);
        this.baseKey.clickon(savebuttonlabel);
    }    

    update_table_Data(tableContentValue,label,value,ButtonLabel){

        this.webtable_update_of_suppliers(tableContentValue);
        this.baseKey.inputTypeIn(label,value);
        this.baseKey.clickon(ButtonLabel);
    }


    /**
     * created by :Arivarasan A
     * Methode Description: validate the given data in the supplier table.
     * 
     * @param {data to be availed in the table} tableContentValue 
     */

    webtable_validation_of_suppliers(tableContentValue) {
        let pathOfRow = "[role='row']";
        let pathofCell = "[data-automationid='DetailsRowCell']>span";

        cy.get(pathOfRow).find(pathofCell).each((tablevalue) => {
            if (tablevalue.text() == tableContentValue) {
                cy.log("validation gets successfull:" + tablevalue.text());  
                // this.baseKey.clickon("Export CSV");  
            }
        });
        
        
    }

    /**
     * created by :Arivarasan A
     * Methode Description: update the supplier table data based upon the data providing.
     * 
     * @param {data to be availed in the table} tableContentValue 
     */

    webtable_update_of_suppliers(tableContentValue) {
        let pathOfTable="[role='presentation']"
        let pathOfRow = "[role='row']";
        let pathofCell = "[data-automationid='DetailsRowCell']>span";
        let editfield="//div[@data-automationid='DetailsRowCell']//span[text()='"+tableContentValue+"']/parent::div/parent::div//following-sibling::div/div/button//i[@data-icon-name='Edit']";
            cy.get(pathOfTable).find(pathOfRow).find(pathofCell).each((tablevalue) => { 
                if (tablevalue.text() == tableContentValue) {
                    cy.log("validation gets successfull:" + tablevalue.text()); 
                    cy.xpath(editfield).click({force:true}); 
                }
            }); 
    }


    /**
     * created by :Arivarasan A
     * Methode Description: Delete the supplier table data based upon the data providing.
     * 
     * @param {data to be availed in the table} tableContentValue 
     */
    webtable_Delete_of_suppliers(tableContentValue) {
        let pathOfTable="[role='presentation']"
        let pathOfRow = "[role='row']";
        let pathofCell = "[data-automationid='DetailsRowCell']>span";
        let editfield="//div[@data-automationid='DetailsRowCell']//span[text()='"+tableContentValue+"']/parent::div/parent::div//following-sibling::div/div/button//i[@data-icon-name='Delete']";
            cy.get(pathOfTable).find(pathOfRow).find(pathofCell).each((tablevalue) => { 
                if (tablevalue.text() == tableContentValue) {
                    cy.log("validation gets successfull:" + tablevalue.text()); 
                    cy.xpath(editfield).click({force:true}); 
                }
            });
           // this.baseKey.clickon("Export CSV");
    }     
    

    /**
     * created by :Arivarasan A
     * Methode Description: search the supplier table data based upon filter option available with the data providing.
     * @param {option available in the filter} optionvalue 
     * @param {search the specific field in the filter} searchfieldLabel 
     * @param {data to validate with table data } tablevalue 
     */
     
    searchAndValidate(optionvalue,searchfieldLabel,tablevalue){
        let path = "//span[text()='"+searchfieldLabel+"']//parent::span//parent::button//parent::div//preceding-sibling::div//span[@role='option']";
        let pathOfOption = "//button//child::span[text()='" + optionvalue + "']";
        let searchfiled="//span[text()='"+searchfieldLabel+"']//parent::span//parent::button//parent::div//preceding-sibling::div//child::input";
        if (cy.xpath(path).scrollIntoView().should("be.visible")){
            cy.xpath(path).scrollIntoView().eq(0).click({ force: true });
            if (cy.xpath(pathOfOption).scrollIntoView().should("be.visible")) {
                cy.xpath(pathOfOption).scrollIntoView().eq(0).click({ force: true });
                cy.xpath(searchfiled).type(tablevalue);
                this.webtable_validation_of_suppliers(tablevalue);
            }
        }
    }


    

    /** created by Arivarasan.
     * methode description: click the respection field.
     * @param {used to click the webelement in the application} label
     */
    fileUpload(label, filepath) {
        let path = "//label[normalize-space(text())='" + label + "']";
        if (cy.xpath(path).scrollIntoView().should('be.visible')) {
            cy.xpath(path).scrollIntoView().selectFile(filepath);
        }
    }
    

    /**created by Arivarasan.
     * methode description: select the date from the datePicker.
     * @param {date labe} label 
     * @param {month of year} month 
     * @param {year} year 
     * @param {date in month} date 
     */
    selectDate(label, month, year, date) {
        let path = "//label[normalize-space(text())='" + label + "']//following-sibling::div/input";
        let monthyear = "[id*='DatePickerDay-monthAndYear']";
        let changethemonthandyear = "[title*='Go to next month']>i";
        let datepick = "td>button[class*='DatePicker-day']";
        var available_monthyear;
        if (cy.xpath(path).scrollIntoView().should("exist")) {
            cy.xpath(path).eq(0).click();
            cy.get(monthyear).then((value) => {
                available_monthyear = value.text().split(/\s/g);
                cy.log("check", available_monthyear[0] + "=" + month);
                //  while(!((available_monthyear[0]==month)&&(available_monthyear[1]==year))){
                //     cy.log("checking in the loop",available_monthyear[0]);
                //     cy.get(changethemonthandyear).click({force:true});
                //     cy.get(monthyear).then((value)=>{
                //      available_monthyear=value.text().split(/\s/g);
                //     });    
                // }
                cy.get(datepick).find("span").each((datevalue) => {
                    //cy.log("check222"+date,datevalue.text());
                    if (parseInt(datevalue.text()) == parseInt(date)) {
                       cy.wrap(datevalue).click();
                       return false;
                    }
                });
            });
        }
    }
}

module.exports = creatingSupplier;