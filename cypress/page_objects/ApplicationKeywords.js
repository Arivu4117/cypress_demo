///<reference types="cypress"/>
const moment=require('moment');

class ApplicationKeywords {


  //textvalue;
  /** created by Arivarasan.
   * methode description: enter the value in input text field.
   * @param {used to locate the input field in the application} locator 
   * @param {value that is enter in the input fields} value 
   * @returns value in the input field
   */

  inputTypeIn(label, value) {
    let path = "//label[normalize-space(text())='" + label + "']//following-sibling::div/input | //label[normalize-space(text())='" + label + "']//following-sibling::div/textarea";
    if (cy.xpath(path).scrollIntoView().should("be.visible")) {
      cy.xpath(path).eq(0)
        .click()
        .clear()
        .type(value, { force: true });
      return value;
    }else{
      cy.log("locator is not present ")
    }
  }

  /** created by Arivarasan.
   * methode description: click the respection field.
   * @param {used to click the webelement in the application} label
   */
  clickon(label) {
    let path = "//button//span[text()='" + label + "'] | //span[normalize-space(text())='Contact persons']//parent::div//following::div/h3[text()='" + label + "']//following-sibling::div/button |//button//following-sibling::a/span[text()='" + label + "']";
    if (cy.xpath(path).scrollIntoView().should('be.visible')) {
      cy.xpath(path).scrollIntoView().click({ force: true });
    }
    return this;
  }

  /** created by Arivarasan.
  * methode description: select the respective option in the field.
  * @param {used to locate the label of the fields in the application} label
  * @param {used to select the value in the option} value  
  */
  selectDropdown(label, value) {
    let pathofField = "[id='" + label + "']>span[role='option']";
    let pathofvalue = "//button//span[normalize-space(text())='" + value + "']";
    if (cy.get(pathofField).scrollIntoView().should("exist")) {
      cy.get(pathofField).scrollIntoView().click();
      if (cy.xpath(pathofvalue).invoke('show')) {
        cy.xpath(pathofvalue).click({ force: true });
      }
    }
  }

  /** created by Arivarasan.
   * methode description: launch the app url.
   * @param {used to launch the application} url
   * @param {used to validate the title of the application} expectedtitle  
   */
  launchapplication(url, expectedtitle) {
    cy.visit(url);
    cy.title().should('eq', expectedtitle);
  }

  /** created by Arivarasan.
    * methode description: generate the date with respect to IST FORMAT.
    * @param {to get future or past date} noofdays (eg: 1 or -1)
    */
  generateDate(noofdays) {
    const UTCdate = new Date(); //getting current date
    UTCdate.setDate(UTCdate.getDate() + noofdays);
    UTCdate.getTime() + (5.5 * 3600000); //Converted UTC TO IST, IST =UTC+5.5 hours
    let isttime = UTCdate.toJSON().slice(0, 10).split("-").reverse();
    let dateFormat = isttime[2] + "/" + isttime[1] + "/" + isttime[0];
    return dateFormat;
  }

  /** created by Arivarasan.
 * methode description: generate the date with respect to IST FORMAT.
 * @param {to generate current date}  (eg: todays date)
 */
  generateDate() {
    return moment().format('DD/MM/YYYY');
  }



  /** created by Arivarasan.
     * methode description: enter the value in input text field.
     * @param {used to locate the input field in the application} locator 
     * @param {value that is enter in the input fields} value 
     * @returns value in the input field
     */

  checkBox(label) {
    let path = "//span[text()='" + label + "']//preceding-sibling::div/i";
    if (cy.xpath(path).scrollIntoView().should("exist")) {
      cy.xpath(path).scrollIntoView().eq(0)
        .click({ force: true })
    }
  }


  /** created by Arivarasan.
   * methode description: generate the date with respect to IST FORMAT.
   * @param {To get the text of the weblement}locator.
   */

  getElementvalue(locator) {
    let textvalue;
    if (cy.xpath(locator, { timeout: 10_000 }).should('be.visible')) {
      return new Promise((resolve, reject) => {
        cy.xpath(locator).then(($value) => {
          textvalue = $value.text();
          cy.log(typeof (textvalue));
          return resolve(textvalue);
        });
      });
    }
  }

  /**created by Arivarasan A
   * Methode description: get the month in string
   * @param {month in integer} month 
   * @returns 
   */
 
 monthinString(month){

    switch (month) {
        case "01":return "January"
            break;
        case "02":return "February"
            break;
        case "03":return "March"
            break;
        case "04":return "April"
            break;
        case "05":return "May"
            break;
        case "06":return "June"
            break;
        case "07":return "July"
            break;
        case "08":return "August"
            break;
        case "09":return "September"
            break;
        case "10":return "October"
            break;
        case "11":return "November"
            break;
        case "12":return "November"
            break;
    
        default:
            break;
    }
  }
}

module.exports = ApplicationKeywords;