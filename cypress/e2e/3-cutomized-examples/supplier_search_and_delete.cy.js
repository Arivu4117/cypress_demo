const creatingSupplier=require("../../page_objects/supplier");
const ApplicationKeywords=require('../../page_objects/ApplicationKeywords');

describe("delete and validate the suppliers",()=>{
    const supplier=new creatingSupplier();
    const basekeywords=new ApplicationKeywords();

    let applicationCredentialData;
    let applicationData;

    before("getting the testdata",()=>{
        
        cy.fixture('properties').then((data)=>{
          applicationCredentialData=data;
        });
        
        cy.fixture('suppliers').then((data)=>{
            applicationData=data;
        });
    });

    beforeEach("launching the Application",()=>{
        basekeywords.launchapplication(applicationCredentialData.application_url,applicationCredentialData.application_title);
    });


    it("checking the suppliers after deleting some information",()=>{
        supplier.webtable_Delete_of_suppliers(applicationData.external_name);
        basekeywords.clickon(applicationData.export_supplierlabel);
    });


    it("checking the availed supliers",()=>{
        supplier.searchAndValidate(applicationData.external_namelabel,applicationData.supplierlabel,applicationData.external_name_value);
        basekeywords.clickon(applicationData.export_supplierlabel); 
    });

    after("Adding Additional Details to the report",()=>{
        cy.allure().writeEnvironmentInfo({
           "Browser": JSON.stringify( Cypress.browser.displayName),
           "OS": JSON.stringify( Cypress.arch),
           "Headed": JSON.stringify( Cypress.browser.isHeaded),
           "Headless": JSON.stringify( Cypress.browser.isHeadless),
        });
      });
});