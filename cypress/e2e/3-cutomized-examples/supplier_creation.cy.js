const creatingSupplier = require("../../page_objects/supplier");
const ApplicationKeywords = require('../../page_objects/ApplicationKeywords');
const { info } = require("winston");

// const logger = require('../../page_objects/logger')

describe("creating the suppliers", () => {
    const supplier = new creatingSupplier();
    const basekeywords = new ApplicationKeywords();

    let applicationCredentialData;
    let applicationData;

    before("getting the testdata", () => {
        //logger.info("getting the data from fixtures as fixed data");
        cy.fixture('properties').then((data) => {
            applicationCredentialData = data;
        });
        cy.fixture('suppliers').then((data) => {
            applicationData = data;
        });
    });
    beforeEach("launching the Application", () => {
        basekeywords.launchapplication(applicationCredentialData.application_url, applicationCredentialData.application_title);
    });

    it("checking of creating the suppliers", () => {

    //     cy.log('This is a log message').then((log) => {
            
    //   cy.task('writeToLog', log);
    // });
        // logger.info("after the klogg");
        cy.log('This is a log message',{ level: 'info' });
        supplier.creationOfSupplier(applicationData.supplierlabel, applicationData.internal_name, applicationData.internal_name_value, applicationData.external_namelabel, applicationData.external_name,
            applicationData.Description_label, applicationData.Description_value, applicationData.industry_option_label, applicationData.industry_option_value,
            applicationData.no_of_employee_label, applicationData.no_of_employee_value, applicationData.date_label,
            applicationData.address1_label, applicationData.address1_value, applicationData.address2_label, applicationData.address2_value,
            applicationData.city_label, applicationData.city_value, applicationData.state_label, applicationData.state_value, applicationData.zip_label,
            applicationData.zip_value, applicationData.fileuplaod_label, applicationData.filepath_value, applicationData.cfo_name, applicationData.ceo_name,
            applicationData.hr_name, applicationData.administrator_name, applicationData.recurrent_Supplierlabel, applicationData.save_button_label);
        supplier.webtable_validation_of_suppliers(applicationData.external_name);
        supplier.update_table_Data(applicationData.externaltoUpdate, applicationData.external_namelabel, applicationData.externalforupadte, applicationData.save_button_label);
        basekeywords.clickon(applicationData.export_supplierlabel);

       
           // cy.task('writeToLog', "write somithing");
          
    });

    after("Adding Additional Details to the report", () => {
        cy.allure().writeEnvironmentInfo({
            "Browser": JSON.stringify(Cypress.browser.displayName),
            "OS": JSON.stringify(Cypress.arch),
            "Headed": JSON.stringify(Cypress.browser.isHeaded),
            "Headless": JSON.stringify(Cypress.browser.isHeadless),
        });
        
    });
});