const { defineConfig } = require("cypress");
const allureWriter = require('@shelex/cypress-allure-plugin/writer');
const moment = require('moment');
const path = require('path');
const fs = require('fs');



module.exports = defineConfig({

  "downloadsFolder": 'cypress/file/' + moment().format("DD-MM-YYYY-HH-MM-SS"),
  "videosFolder": "cypress/videos/" + moment().format("DD-MM-YYYY-HH-MM-SS"),
  e2e: {
    env: {
      "allureResultsPath": `cypress/results/allure/allure-result`,
    },
    retries: {
      runMode: 1,
      openMode: 1,
    },
    setupNodeEvents(on, config) {
      // const options = {
      //   outputRoot: 'cypress/logs/'+moment().format("DD-MM-YYYY-HH-MM-SS"),
      //   printLogsToConsole: "always",
      //   printLogsToFile: "always",
      //   outputTarget: {
      //     'out.txt': 'txt',
      //     'out.json': 'json',
      //   }
      // };

      require('cypress-terminal-report/src/installLogsPrinter')(on, {
        printLogsToConsole: "always",
      });
      allureWriter(on, config);
      // require('cypress-failed-log/on')(on);
      // require('cypress-log-to-output').install(on)
      return config;
    },
    // implement node event listeners here
  },
});
